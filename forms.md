### Barrel Development Best Practices

# Form Handling Best Practices

When possible, use a well-built library, plugin, or module for form management within a CMS. 

For WordPress, we currently recommend [Gravity Forms](http://www.gravityforms.com/) as it provides a clean API for manipulating form submissions and extending the field types available. In cases where a CMS is not possible, then solutions like TypeForm, JotForm, or Google Forms are also recommended. While most projects will apply a custom theme to form fields, please select a system that is compatible with the target form management solution.

## Frontend

### Validation and Error Messages

### Native Controls

### Accessibility

### Responsive

### Submissions

#### XML and POST HTTP Requests
POST and AJAX requests should provide relevant feedback for failed submissions as well as successful ones. Errors should be displayed in aggregate or per field and inline. User feedback like a "Thank You" page or message should be displayed to the end user upon successful submission. The History API should change the URL and fire any event tracking requests when requested over AJAX.

#### Security
Appropriate system-level security tokens (nonces) should be distributed and validated on the backend. All submissions should occur over an HTTPS connection and should survive basic penetration tests without exposing sensitive data.